import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

import Navbar from '../components/Navbar'
import Subscribe from '../components/Subscribe'
import Footer from '../components/Footer'
import './all.sass'

const TemplateWrapper = ({ children }) => (
  <Fragment>
    <Helmet title="Home | Shift Tribe">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i|Spectral:400&amp;subset=latin-ext" rel="stylesheet" />
      <link rel="stylesheet" type="text/css" href="/assets/css/normalize.css" />
      <link rel="stylesheet" type="text/css" href="/assets/css/screen.css" />
      <link rel="stylesheet" type="text/css" href="/assets/css/icons.css" />
    </Helmet>
    <Navbar />
    <Fragment>
      {children()}
    </Fragment>

    <Subscribe />
    <Footer />

    <script src="/assets/js/global.js"></script>
    <script src="/assets/js/home.js"></script>
  </Fragment>
)

TemplateWrapper.propTypes = {
  children: PropTypes.func,
}

export default TemplateWrapper
