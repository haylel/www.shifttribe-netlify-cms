---
templateKey: blog-post
title: Shift, how we got started and where we're going
date: 2018-06-16T12:21:55.000Z
author: Chloe Guilhermino
description: The practice of mindfulness if one of the most powerful tools we have available to us today that allows us to become conscious of ourselves as the creators of our own lives.
tags:
  - mindfulness
  - meditation
  - peace
  - gathering
---

Hello Beautiful People

Thank you for joining the Tribe. This blog is to share a little more with you about how we got started and where we as an organisation aim to go.

Shift, originally Paradigm Shift was started in 2013 with the simple intention of getting a few friends together to meditate for peace. What started out as a facebook event for a few friends and family soon went semi-viral and the first event ended up having over 50 people attend.

![The beginning](/assets/images/blog-how-we-started.jpg)

Through this the founder, Ashley Epstein, saw an incredible need for community gathering and the need for people to come together in a sense of peace, love and unity.

Since then we have continued with our Full Moon Beach Meditations on Camps Bay Beach Cape Town and has now grown into the biggest world peace meditation event in the Southern Hemisphere, now bringing together over 1,500 people.

The intention behind these gathering is based on the Maharishi Effect, where by studies have been done in various cities around the world and found that cities where people actively meditated together had a reduced crime rate and a deeper sense of community and well-being. When people come together in a unified intention such a peace, love and unity, this intention ripples out into the world to bring profound change and healing on a global level.

Our vision is to take these events around the world to eventually be able to co-ordinate a global peace meditation.

Through our full moon events we have grown into offering various mindful events, classes and workshops and now our main focus now being to bring this incredible knowledge online and available at a global level.

At Shift we believe that world peace can be achieved if each person becomes peaceful within themselves and mindful practice is one of the most powerful tools we have available to us today that allows us to consciously and continuously Shift ourselves into a higher, more aligned and peaceful state of being.
It is our deepest wish to see and experience a unified, peaceful and sustainable world where each one of us lives in the joy and celebration of our own existence.

Together we truly are the change, but that change needs to start at an individual level by each and every one of us taking responsibility for our lives, our own happiness and what we are contributing to this precious world of ours.

Shift Tribe hopes to great a global online conscious community of shared learning, growth, peace and love.

We thank each and every one of you for being on this journey with us and we are so excited for all that is to come.

Infinite blessing and love to you xx

Written by Chloe Guilhermino
Co-founder of Shift
