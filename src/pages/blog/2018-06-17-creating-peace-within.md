---
templateKey: blog-post
title: Meditation and creating peace within.
date: 2018-06-16T12:21:57.000Z
author: Chloe Guilhermino
description: Meditation is a tool used by millions of people around the world.
tags:
  - meditation
  - focus
---

Meditation is a tool used by millions of people around the world.

Consistent practice of meditation allows you to sharpen your ability to focus on the present moment, thereby effortlessly unloading your mind from all the thoughts and stimulation that you process on a daily basis. Meditation alleviates the pressure and consumption of the past or the future – which are the places in your mind where we mostly experience stress or anxiety.

Every single moment we experience is one of a kind – it will never happen again or be experienced by anyone else in all of existence. It is an once-in-a-lifetime gift just for you. When you become truly present in the moment you are in, you come to realize the uniqueness, simplicity and beauty of each moment.

Meditation teaches us how to bring our minds into our body and into our senses so that we can begin to experience life through our senses rather than through the perception of the mind. This is so that we may become aware of, and in tune with, the moment we are in. In this way, we are able really to taste the food we are eating, hear the person talking to us, feel what we are feeling, and see and appreciate all of who we are.

Through constant and focused practice, we will effortlessly become aware of the preciousness, beauty and gift that each moment holds. Your life will become a song unto itself as you savour and appreciate every moment and experience it for the perfection that it is. It is in this space that there can be no fear, stress or frustration.

There are so many different forms of meditation and so it is really about trying them out and finding what sort of mindful practice works best for you.

![Meditation](/assets/images/blog-meditation-post.jpg)

Shift Tribe has just started a youtube channel that provides free online content of various forms of mindful practices and so is a great place to start.

Take a deep breath in, come in to the moment here and now... Become aware of the magic, become aware of the beauty of your own existence.

Much love,
Shift Tribe
