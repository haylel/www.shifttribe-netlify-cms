import React from 'react'
import PropTypes from 'prop-types'
import { RetreatsPageTemplate } from '../../templates/retreats-page'

const RetreatsPagePreview = ({ entry, widgetFor }) => (
  <RetreatsPageTemplate
    title={entry.getIn(['data', 'title'])}
    content={widgetFor('body')}
  />
)

RetreatsPagePreview.propTypes = {
  entry: PropTypes.shape({
    getIn: PropTypes.func,
  }),
  widgetFor: PropTypes.func,
}

export default RetreatsPagePreview
