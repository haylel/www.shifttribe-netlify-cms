import CMS from 'netlify-cms'

import AboutPagePreview from './preview-templates/AboutPagePreview'
import BlogPostPreview from './preview-templates/BlogPostPreview'
import RetreatsPagePreview from './preview-templates/RetreatsPagePreview'

CMS.registerPreviewStyle('/styles.css')
CMS.registerPreviewTemplate('about', AboutPagePreview)
CMS.registerPreviewTemplate('retreats', RetreatsPagePreview)
CMS.registerPreviewTemplate('blog', BlogPostPreview)
