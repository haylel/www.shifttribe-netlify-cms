import React from 'react'
import Link from 'gatsby-link'

import SocialIcons from './SocialIcons';

const Footer = () => (
  <footer className="footer-wrap">
  	<div className="footer-social">
      <SocialIcons />
  	</div>
  	<div className="footer-copyright">
  		Copyright © 2018, <a href="http://www.shifttribe.com">Shift Tribe</a>. All Right Reserved.
  		<br/>
  	</div>
  </footer>
)

export default Footer
