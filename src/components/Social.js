import React from 'react'
import Link from 'gatsby-link'

const Social = () => (
<div className="post-meta" style={{ paddingBottom: '2em' }}>
		<div className="post-share">
			<a href="https://twitter.com/intent/tweet?text=A%20mind%20cannot%20be%20independent%20of%20culture&amp;url=http://shift.ghost.io/a-mind-cannot-be-independent-of-culture/" onClick="window.open(this.href, 'twitter-share', 'width=550,height=235');return false;"><i className="twitter"></i>
			</a><a href="https://www.facebook.com/sharer/sharer.php?u=http://shift.ghost.io/a-mind-cannot-be-independent-of-culture/" onClick="window.open(this.href, 'facebook-share','width=580,height=296');return false;"><i className="facebook"></i>
			</a><a href="https://plus.google.com/share?url=http://shift.ghost.io/a-mind-cannot-be-independent-of-culture/" onClick="window.open(this.href, 'google-plus-share', 'width=490,height=530');return false;"><i className="google-plus"></i></a>
	</div>
</div>
)

export default Social
